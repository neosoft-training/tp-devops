# TP - Multistage

L’objectif de ce TP est de découvrir les avantages de la philosophie Multi-stage proposée par Docker.

## Notre classe JAVA

- D'abord, nous allons commencer par l'écriture d'une classe java basique qui permet d'afficher le message "Bienvenue à la formation docker/kubernetes"

<details>
<summary>Astuce</summary>

> Le fichier `Formation.java` existe déjà dans le répertoire `sources`

</details>

<details>
<summary>La solution</summary>

```bash
class Formation {
   public static void main(String[] a) {
       System.out.println("Bienvenue à la formation docker/kubernetes");
   }
}
```

</details>

## Ecriture de notre premier Dockerfile

- Ce Dockerfile doit réaliser les actions ci-dessous:
    - récuperer l'image de base `openjdk:11-jdk` 
    - copier la classe Formation.java
    - compiler la classe java en utilisant la commande `javac`
    - lancer la commande d'entrée `java Formation`

<details>
<summary>La solution</summary>

```bash
FROM openjdk:11-jdk
COPY Formation.java .
RUN javac Formation.java
CMD ["java","Formation"]
```

</details>

## Builder notre première image

- Builder l'image `formation` avec le tag `before`

<details>
<summary>La commande</summary>

> La solution ? Aaaaah Tu n'a pas bien révisé alors ^_^ çava rester entre nous. shuuuut !!!

<details>
<summary>La commande</summary>
    
```bash
docker build -t formation:before .
```

</details>
</details>


## Ecriture de notre deuxième Dockerfile en utilisant Multi-stage

- Ce Dockerfile doit réaliser les actions ci-dessous:
    - Stage 1 :
        - récuperer l'image de base `openjdk:11-jdk` 
        - copier la classe Formation.java
        - compiler la classe java en utilisant la commande `javac`
    - Stage 2 :
        - récuperer l'image de base `openjdk:11-jre-slim` 
        - copier le fichier Formation.class déjà généré dans le premier stage
        - lancer la commande d'entrée `java Formation`

> [!NOTE]
> N'oublies pas d'ajouter un petit commentaire au début de chaque stage afin de séparer la partie `Compilation` de la partie `Execution`

> [!NOTE]
> Le fichier Dockerfile doit être nommé DockerfileOptimized

<details>
<summary>Astuce</summary>

N'oublies pas d'utiliser les alias `AS xyz`

</details>

<details>
<summary>La solution</summary>

```bash
# Stage: Compilation
FROM openjdk:11-jdk AS build
COPY Formation.java .
RUN javac Formation
# Stage: Execution
FROM openjdk:11-jre-slim AS run
COPY --from=build Formation.class .
CMD java Formation
```

</details>

## Builder notre deuxième image

- Lancer le build de l'image `formation` avec le tag `after`

<details>
<summary>La commande</summary>
```bash
docker build -t formation:after -f DockerfileOptimized .
```
</details>



## Comparer les images générés

- D'abord, nous allons vérifier la taille des deux images 

<details>
<summary>La commande</summary>

```bash
docker images
```

</details>

> il y a une différence ?

- Maintenant, nous allons comparer le nombre de couches ajoutées.

<details>
<summary>La commande</summary>

```bash
docker history formation:before
docker history formation:after
```

</details>

> il y a une différence ?



