## Créer un ReplicaSet

### Avec le mode déclaratif

Utilisez le fichier `2_replicaset.yaml` et la commande `kubectl create` pour créer votre replicaset

```bash
kubectl create -f 2_replicaset.yaml
```

### Modifier le nombre de replicas

Utilisez `kubectl scale` pour augmenter le nombre de replicas

> Vous pouvez maintenant le supprimer avec `kubectl delete`