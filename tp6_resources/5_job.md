## Créer un Job

### Avec le mode impératif 

Avec la commande `kubectl create job`, créez votre premier job avec l'image *busybox* et qui execute la commande "echo mon tajine est en cours de préparation;sleep 30;echo mon tajine est enfin prêt"

```bash
kubectl create job tajine --image=busybox -- /bin/sh -c "echo mon tajine est en cours de préparation;sleep 30;echo mon tajine est enfin prêt"
```

Afficher les logs du job créé en attendant minimum 40 secondes, de manière à ce que les deux lignes soient affichées (vous pouvez utiliser l'option -f )


### Avec le mode déclaratif

Afin de simplifier cette étape, nous allons créer notre fichier `5_job.yaml` en utilisant le mode impératif. Ce fichier sera ensuite utilisé dans le cadre du mode déclaratif.

Dans le cadre de cet exercice, nous visons à créer un job nommé *couscous* en utilisant l’image *busybox*. Ce job exécutera la commande suivante en boucle : 'while true; do echo "encore quelques secondes"; sleep 10; done'. Ainsi, tant que notre couscous n’est pas prêt, le message “encore quelques secondes” sera affiché en continu.

Pour créer ce manifest, utilisez la commande suivante :
```bash
kubectl create job couscous --image=busybox --dry-run=client -o yaml -- /bin/sh -c 'while true; do echo encore quelques secondes; sleep 10;done' > 5_job.yaml
```

vérifions d'abord si notre fichier 5_job.yaml n'est pas vide.

Avant de lancer notre job, nous allons ajouter une instruction qui le forcera à s’arrêter si son exécution dépasse 30 secondes

```bash
activeDeadlineSeconds: 30
```

Rencontrez-vous des difficultés pour déterminer où ajouter notre instruction 

<details>
<summary>Astuce</summary>

>  Je vous recommande de consulter la documentation officielle de kubernetes

https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/job-v1/#JobSpec

</details>

En ajoutant la deuxième instruction `completions`, vous pourrez exécuter le même travail cinq fois de suite.

```bash
completions: 5
```

En remplaçant désormais notre instruction de complétion par une autre instruction, vous pourrez exécuter le même travail cinq fois simultanément

```bash
parallelism: 5
```

afin de vérifier l'état de notre job, vous pouvez utiliser la commande suivante :

```bash
kubectl get job
```

Il est temps de supprimer les jobs que nous avons créés

```bash
kubectl delete job tajine couscous
```

## Créer un Cronjob

Avec la commande `kubectl create cronjob`, créez votre premier cronjob summer avec l'image *busybox* et qui execute la commande "date; echo C'est bientôt l'été" en précisant que ce job sera lancer toute les 2 minutes.

```bash
kubectl create cronjob summer --image=busybox --schedule="*/2 * * * *" -- /bin/sh -c "date;  echo C'est bientôt l'été"
```

