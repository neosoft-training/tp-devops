## Créer un DaemonSet

### Avec le mode déclaratif

Utilisez le fichier `4_daemonset.yaml` et la commande `kubectl create` pour créer votre daemonSet

```bash
kubectl create -f 4_daemonset.yaml
```

> Vous pouvez maintenant le supprimer avec `kubectl delete`