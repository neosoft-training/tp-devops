## Créer un Ingress

### Avec le mode déclaratif

Nous allons maintenant créer un Ingress pour exposer votre service à l'extérieur de votre cluster

Utilisez le fichier `7_ingress.yaml` et la commande `kubectl create` pour créer votre Ingress

```bash
kubectl create -f 7_ingress.yaml
```

Interrogez maintenant votre service avec le DNS de votre instance, sur votre navigateur

Bravo, vous venez d'exposer votre application sur Internet !