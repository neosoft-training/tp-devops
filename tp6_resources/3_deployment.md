## Créer un Deployment

### Avec le mode impératif 

Avec la commande `kubectl create`, créez votre premier deployment

```bash
kubectl create deployment test-${USER} --image=nginx
```

Cherchez l'IP du Pod créé par votre Deployment avec `kubectl describe pod` puis effectuez un `curl` sur cette IP

```bash
kubectl describe pod
curl http://<ip>
```

Vous devriez voir la page d'accueil NGinx !

> Vous pouvez maintenant le supprimer avec `kubectl delete`

### Avec le mode déclaratif

Utilisez le fichier `3_deployment.yaml` et la commande `kubectl create` pour créer votre 2nd deployment

```bash
kubectl create -f 3_deployment.yaml
```

### Rollout

Afficher l'état de notre déploiement training-${USER}-deploy

```bash
kubectl rollout status deploy training-${USER}-deploy
```

Afficher l'historique de notre déploiement training-${USER}-deploy

```bash
kubectl rollout history deploy training-${USER}-deploy
```
l'historique est vide ? C'est normal.

mettant à jour la version de notre image nginx via la ligne de commande

```bash
kubectl set image deploy training-${USER}-deploy hello-app=gcr.io/google-samples/hello-app:2.0
```

ou bien via l'éditeur vim en modifiant directement le fichier yaml de notre déploiement

```bash
kubectl edit deploy training-${USER}-deploy
```

Vérifions l'état de notre déploiement 

```bash
kubectl rollout history deploy training-${USER}-deploy
kubectl get deploy training-${USER}-deploy
kubectl get po
```

mettant à jour la version de notre image nginx  mais cette fois avec une version erronée, via la ligne de commande

```bash
kubectl set image deploy training-${USER}-deploy hello-app=gcr.io/google-samples/hello-app:3.0
```

Vérifions l'état et l'historique de notre déploiement 

```bash
kubectl rollout status deploy training-${USER}-deploy
kubectl rollout history deploy training-${USER}-deploy
kubectl get deploy training-${USER}-deploy
kubectl get po
```
### Rollback

Si la version est KO, il faut prévoir un retour arrière mais d'abord il faut trouver la bonne version via la commande suivante

```bash
kubectl rollout history deploy training-${USER}-deploy --revision=2
```

maintenant, il faut faire un retour arrière pour garantir le bon fonctionnement de notre déploiement test-${USER}

```bash
kubectl rollout undo deploy training-${USER}-deploy --to-revision=2
```

Vérifions l'état et l'historique de notre déploiement 

```bash
kubectl rollout status deploy training-${USER}-deploy
kubectl rollout history deploy training-${USER}-deploy
kubectl get deploy training-${USER}-deploy
kubectl get po
```

### Modifier le nombre de replicas
Utilisez `kubectl scale` pour augmenter le nombre de replicas

> Conservez ce Deployment qui nous servira pour la suite
