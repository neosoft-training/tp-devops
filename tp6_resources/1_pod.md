## Créer un Pod

### Avec le mode impératif 

Avec la commande `kubectl run`, créez votre premier pod

```bash
kubectl run test-${USER} --image=nginx
```

Cherchez l'IP du Pod avec `kubectl describe test-${USER}` puis effectuez un `curl` sur cette IP

```bash
kubectl describe pod test-${USER}
curl http://<ip>
```

Vous devriez voir la page d'accueil NGinx !

> Vous pouvez maintenant le supprimer avec `kubectl delete`

### Avec le mode déclaratif

Utilisez le fichier `1_pod.yaml` et la commande `kubectl create` pour créer votre 2nd pod

Générer un manifest avec cette commande :
```bash
kubectl run training-${USER} --image=nginx --dry-run=client -oyaml > 1_pod.yaml
```

Puis l'appliquer :

```bash
kubectl create -f 1_pod.yaml
```

## Finalement, ma page “Ramadan” est en ligne

Hier, j’ai animé une formation sur Docker/Kubernetes. À ma grande surprise, mes collègues de travail n’étaient pas au courant que le Ramadan avait déjà débuté. J’ai donc pris l’initiative de créer une page spécialement pour les informer. Cependant, j’ai rencontré quelques difficultés et j’aurais besoin d’aide. Je compte sur vous pour :

- élaborer un manifeste ramadan.yaml qui définit notre pod “ramadan”, lequel comprend deux conteneurs:
- un premier container avec l'image "nginx" dont le nom est "mon-serveur"
- un deuxième container du type InitContainer avec l'image "busybox" dont le nom est "init-mon-service", ce container doit créer un fichier index.html contenant le message "Le Ramadan a déjà commencé."
- utiliser un volume éphémère de type emptyDir pour stocker le fichier index.html dans le répertoire "/workspace"
- créer le pod dans le namespace ramadan
- exposer le port 80


C'est bon, vous avez terminé ? Tout est Clair pour Claire ?

Alors, j’aimerais vraiment voir le résultat final. Qui serait prêt à être le premier à me montrer ma page ?


<details>
<summary>Astuce</summary>

> Pour récupérer l'adresse ip du pod, il suffit de lancer la commande

```bash
k -n ramadan get po -o wide
```

</details>

<details>
<summary>La solution</summary>

```bash
apiVersion: v1
kind: Pod
metadata:
  name: ramadan
  namespace: ramadan
spec:
  volumes:
  - name: workdir
    emptyDir: {}
  initContainers:
  - name: init-mon-service
    image: busybox
    volumeMounts:
    - name: workdir
      mountPath: /workspace
    command: ["/bin/sh", "-c"]
    args: ["echo 'Le Ramadan a déjà commencé.' > /workspace/index.html"]
  containers:
  - name: mon-serveur
    image: nginx
    ports:
    - containerPort: 80
    volumeMounts:
    - name: workdir
      mountPath: /usr/share/nginx/html
```
</details>


## Ils sont où mes logs ?

Notre collègue Philippe a récemment eu l’opportunité d’implémenter la solution ELK sur nos serveurs.
Dans le but de partager ses compétences avec nous, il a conçu un exercice pratique pour nous permettre de nous exercer.

L'objectif est de :
- élaborer un manifeste ELK.yaml qui définit notre pod "elk", lequel comprend deux conteneurs:
- un premier container avec l'image "nginx" dont le nom est "mon-serveur"
- un deuxième container avec l'image "busybox" dont le nom est "logger", ce container doit stocker les logs de notre serveur nginx en live  tant que le serveur est toujours fonctionnel"
- utiliser un volume éphémère de type emptyDir pour stocker le fichier access.log dans le répertoire "/var/log/nginx"
- créer le pod dans le namespace theia
- exposer le port 8080


Maintenant, j’ai vraiment hâte de voir le produit fini. Phillipe sera certainement impressionné par notre travail !


<details>
<summary>Astuce</summary>

> Pour afficher les logs en live, il faut utiliser la commande tail -f

</details>

<details>
<summary>La solution</summary>

```bash
apiVersion: v1
kind: Pod
metadata:
  name: elk
  namespace: theia
spec:
  containers:
  - name: mon-serveur
    image: nginx
  - name: logger
    image: busybox
    command: ['sh', '-c', 'tail -f /var/log/nginx/access.log']
    volumeMounts:
    - name: log-volume
      mountPath: /var/log/nginx
  volumes:
  - name: log-volume
    emptyDir: {}
```
</details>

<details>
<summary>Une erreur ?</summary>

> Ah, c’est certainement un piège. J’étais convaincu dès le départ que Philippe ne donnerait pas un exercice aussi simple.
  Cependant, pour identifier la cause, il est essentiel de ne pas négliger l’utilisation des commandes ‘describe’ et ‘logs’.

</details>

Maintenant que tous les conteneurs ont démarré avec succès, c’est une excellente nouvelle. Cependant, j’aimerais vérifier si le fichier access.log enregistre réellement les logs en temps réel. Quelqu’un pourrait-il me montrer comment procéder ?

<details>
<summary>Astuce</summary>

> utiliser la commande "exec"

</details>


## Jouons un peu avec les LABELS

Constituez 5 pods nommés nginx1, nginx2, nginx3, nginx4 et nginx5. Chacun d’eux doit porter le label mois=ramadan

- afficher les labels de chaque pod

- Modifier le label du pod ‘nginx2’ pour qu’elle soit mois=mars.

- Afficher une colonne nommée MOIS qui regroupe toutes les valeurs associées aux labels ayant pour clé MOIS.

- Récupérer seulement les pods qui ont le label ‘mois=mars’.

- Attribuer le label annee=2024 à tous les pods qui portent les labels ‘mois=ramadan’ ou ‘mois=mars’..

## Encore un peu avec les ANNOTATIONS

- Ajouter une annotation "ville=paris" à tous les pods ayant le label "mois=mars"

- Supprimez le label "mois" des pods que nous avons créés précédemment

- Ajouter l'annotation "commune='Saint Malo'" aux pods nginx1, nginx2, nginx3, nginx4

- Afficher les annotations du pod nginx1

- Supprimer les annotations de ces 5 pods

- Supprimez tous les pods créés précédemment


## J’ai perdu la trace de mes métadonnées.

Notre coéquipier Laurent est actuellement en train de mettre en place son propre proxy, mais il lui manque certaines informations pour terminer sa configuration. En tant qu’équipe, nous ne le laisserons pas faire cela seul, nous allons l’assister.

La première étape consiste à installer les deux bibliothèques jq et yq si elles ne sont pas déjà présentes sur notre instance EC2.

La deuxième étape consiste à créer un pod avec deux conteneurs (sidecar de préférence), en exposant le port 80 pour le premier conteneur et 8080 pour le deuxième.

L’objectif est d’obtenir la même information en utilisant les 3 bibliothèques, en commençant par :

- Nom du pod
- son namespace
- sa status
- l'adresse ip
- ses annotations
- ses labels
- l'image du premier conteneur
- le nom du premier conteneur
- le port du deuxième conteneur
- la commande lancée par de le 2eme container ( si c'est le cas)
- la politique de démarrage ( restartPolicy)


Cela semble facile, n’est-ce pas ? Maintenant, passons aux choses sérieuses en récupérant :
- la liste des conteneurs avec le nom de chaque conteneur, son adresse IP et son port exposé
- le nombre de conteneurs
- La liste des ports que nos conteneurs exposent, classés en ordre décroissant
- Le port le plus élevé parmi les ports exposés par nos deux conteneurs.

<details>
<summary>Astuce</summary>

> Pour afficher la structure total de notre yaml, il suffit d'utiliser une des options suivantes :
- -o yaml
- -o json | jq '.'
- -o yaml | yq '.' 


</details>


## Examinons maintenant l’état de nos conteneurs

Thomas, notre collègue, n’est pas content du fonctionnement du garbage collector de notre registry. Il a donc décidé de prêter main-forte à l’éditeur.

Après analyse, il a découvert un pod qui ne démarrait pas. En tant qu’équipe, nous ne le laisserons pas gérer cela seul, nous allons le soutenir.

Voici le fichier de notre pod qui rencontre des problèmes.

```bash
apiVersion: v1
kind: Pod
metadata:
  labels:
    test: liveness
  name: liveness-exec
spec:
  containers:
  - name: liveness
    image: registry.k8s.io/busybox
    args:
    - /bin/sh
    - -c
    - touch /tmp/healty; sleep 600
    livenessProbe:
      exec:
        command:
        - cat
        - /tmp/healthy
      initialDelaySeconds: 5
      periodSeconds: 5
```

Maintenant, c’est à vous de déboguer pour identifier le problème et de le résoudre par la suite.

Si le nombre de redémarrages a déjà atteint 6, cela signifie que le test a été échoué. Dans cette situation, il est recommandé de recommencer à partir de zéro en recréant le pod à l’aide de notre fichier yaml.

Maintenant on va tester la même chose mais avec un readiness probe !!

```bash
apiVersion: v1
kind: Pod
metadata:
  labels:
    test: readiness
  name: readiness-exec
spec:
  containers:
  - name: readiness
    image: registry.k8s.io/busybox
    args:
    - /bin/sh
    - -c
    - touch /tmp/healty; sleep 600
    readinessProbe:
      exec:
        command:
        - cat
        - /tmp/healthy
      initialDelaySeconds: 5
      periodSeconds: 5
```


Alors c'est quoi la différence entre les deux probes Readiness et Liveness ?

