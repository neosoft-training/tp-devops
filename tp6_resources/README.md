# TP6 - Déployer des ressources dans Kubernetes

Votre cluster est installé, votre commande kubectl est configurée, il est temps d'installer vos premières ressources !

## Configuration

Avant tout, exportez une variable qui vous servira pour les commandes impératives, en remplaçant les variables par votre utilisateur

```bash
# In tp6_resources/ folder
export USER=$(whoami)
# Replace n every YAML file
sed -i -re 's|\$\{USER\}|'${USER}'|g' *.yaml
```
