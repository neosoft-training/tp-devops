## Créer un namespace

### Avec le mode impératif 

Avec la commande `kubectl create`, créez votre premier namespace

```bash
kubectl create namespace test-${USER} -o yaml
```

La commande vous affiche le Namespace créé, au format YAML

> Vous pouvez maintenant le supprimer avec `kubectl delete`

### Avec le mode déclaratif

Utilisez le fichier `0_ns.yaml` et la commande `kubectl create` pour créer votre 2nd namespace

```bash
kubectl create -f 0_ns.yaml
```

> Conservez ce namespace qui nous servira pour la suite

## Changer de namespace

Nous allons utiliser et persister le namespace créé précédemment :

```bash
kubectl config set-context --current --namespace training-${USER}
```

---
