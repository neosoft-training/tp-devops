# TP-devops

Ce dépôt contient les TPs de la formation "Formation Docker et Kubernetes | Les fondamentaux"


Chaque TP contient :
* l'énoncé 
* l'ensemble des sources nécessaires à la réalisation des exercices
