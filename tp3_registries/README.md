# TP3 - Docker Hub & Registry

Manipulation du registry

## Registry privée

- Comme indiqué sur la [documentation Docker](https://docs.docker.com/registry/deploying/#running-on-localhost), installez un registry privé en local avec la commande suivante

```bash
docker run -d -p 5000:5000 --restart=always --name registry registry:2
```


- En suivant les instructions précédentes et avec l'aide des slides de la partie théorique et de la documentation officielle, essayez de réaliser les tâches suivantes :

### Etape 1: L'authentification

- se connecter au registry (en sachant qu'il n'y a pas d'authentification obligatoire dans notre cas) !! 
    - décoder avec une ligne de commande le mot de passe stocké dans le ficher généré suite à l'authentification
- se déconnecter du registry
    - le fichier généré existe toujours ?

### Etape 2: Stocker notre première image taguée

- récuperer une image (busybox par exemple) depuis le reposity par défaut
- taguer l'image récupérée (busybox) avec le tag 'doudou', sans oublier de préciser le nom du registry
- consulter le registy afin de vérifer qu'il n'y a pas encore d'images dedans.
- poussez l'image que vous avez taguée sur votre registry 
- consulter le registy, si tout fonctionne bien vous devriez avoir une image taguée.

### Etape 3: Stocker plusieurs versions taguées pour la même image

- retaguer la même image récupérée (busybox) avec le tag 'restoMidi', sans oublier de préciser le nom du registry? cela ne passe pas ?
- poussez l'image que vous avez taguée sur votre registry 
- consulter le registy, si tout fonctionne bien vous devriez avoir deux images taguées.
- cela ne fonctionne pas ? dans ce cas à partir du registry,  afficher les tags d’un repository spécifique (busybox par exemple)


