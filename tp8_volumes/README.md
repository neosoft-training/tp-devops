## Créer un PV

Pour créer un persistant volume, nous devons passer obligatoirement par le mode déclaratif, en lancant la commande suivante:

```bash
kubectl create -f 8_pv.yaml
```

Vérifier l'état de notre PV

```bash
k get pv
```

## Créer un PVC

Puis créer un persistant volume claim, pareil aussi nous devons passer obligatoirement par le mode déclaratif, en lancant la commande suivante:

```bash
kubectl create -f 8_pvc.yaml
```

Vérifier l'état de notre PVC

```bash
k get pvc
```

## Créer un Pod

Créer un pod qui utilise notre volume persistant

```bash
k create -f 8_pod.yaml
```